import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { Button, List, ListItem } from 'react-native-elements';

class WallOfShame extends Component {
  handleSettingsPress = () => {
    console.log('wall of shame button is pressed');
  };

  render() {
    return (
      <ScrollView>
        <Button
          title="Post shame"
          buttonStyle={{ marginTop: 20 }}
          onPress={this.handleSettingsPress}
        />
        <List>
          <ListItem
            title="some shame #1"
            rightTitle="some shame #1"
            hideChevron
          />
          <ListItem
            title="some shame #2"
            rightTitle="some shame #2"
            hideChevron
          />
        </List>
      </ScrollView>
    );
  }
}

export default WallOfShame;
