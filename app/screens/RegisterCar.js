import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { Button } from 'react-native-elements';

class RegisterCar extends Component {
  handleSettingsPress = () => {
    console.log('register car button is pressed');
  };

  render() {
    return (
      <ScrollView>
        <Button
          title="Register car"
          buttonStyle={{ marginTop: 20 }}
          onPress={this.handleSettingsPress}
        />
      </ScrollView>
    );
  }
}

export default RegisterCar;
