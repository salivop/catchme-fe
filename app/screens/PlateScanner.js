import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { Tile, Button } from 'react-native-elements';

class PlateScanner extends Component {
  handleSettingsPress = () => {
    console.log('button is pressed');
  };

  render() {
    return (
      <ScrollView>
        <Button
          title="Scan Now"
          buttonStyle={{ marginTop: 20 }}
          onPress={this.handleSettingsPress}
        />
      </ScrollView>
    );
  }
}

export default PlateScanner;
