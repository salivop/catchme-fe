import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

import RegisterCar from '../screens/RegisterCar';
import PlateScanner from '../screens/PlateScanner';
import WallOfShame from '../screens/WallOfShame';

export const Tabs = TabNavigator({
  PlateScanner: {
    screen: PlateScanner,
    navigationOptions: {
      tabBarLabel: 'Scan',
      tabBarIcon: ({ tintColor }) => <Icon name="list" size={35} color={tintColor} />,
    },
  },
  RegisterCar: {
    screen: RegisterCar,
    navigationOptions: {
      tabBarLabel: 'Me',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
  WallOfShame: {
    screen: WallOfShame,
    navigationOptions: {
      tabBarLabel: 'Wall of shame',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />
    },
  },
});

export const Root = StackNavigator({
  Tabs: {
    screen: Tabs,
  }
}, {
  mode: 'modal',
  headerMode: 'none',
});
